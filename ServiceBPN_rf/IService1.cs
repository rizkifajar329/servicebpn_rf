﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ServiceBPN_rf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        //SertifikatWarisan
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "addSertifikatWrs")]
        string inputSertifikatWarisan(SertifikatWarisan dataTanah);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAllSertifikatWrs")]
        List<SertifikatWarisan> getSertifikatWarisan();

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getDataSWaris/{nik_pemohon}")]
        List<SertifikatWarisan> getDataSWarisan(string nik_pemohon);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "deleteDataSWaris")]
        void deleteDataSWaris(SertifikatWarisan data);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "updateDataSWaris")]
        void updateSwaris(SertifikatWarisan data);

        ////////////////////////////////////////////////////////////
        //KetWrs
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAllKetWrs")]
        List<KetWrs> getKetWrs();


        ///Surat Kuasa
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAllSuratKuasa")]
        List<SuratKuasa> getSuratKuasa();


        ///SertifikatAsli
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAllSertifikatAsli")]
        List<SertifikatAsli> getSertifikatAsli();

        ///AktaJB
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAllAktaJB")]
        List<AktaJB> getAktaJB();

        //KetMati
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAllKetMati")]
        List<KetMati> getKetMati();

        ///SSBPHTP
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAllSSBPHTP")]
        List<SSBPHTP> getSSBPHTP();

        //PPH
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAllPPH")]
        List<PPH> getPPH();




    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class SertifikatWarisan
    {
        private string nik_pemohon, nama_pemohon, alamat_pemohon, letak_tanah, surat_kuasa, sertifikat_asli,
            akta_jb, keterangan_warisan, keterangan_kematian, sspbphtp, pph;
        private int luas_tanah;
        private DateTime tanggal;

        [DataMember(Order = 1)] public string NIKPemohon { get => nik_pemohon; set => nik_pemohon = value; }

        [DataMember(Order = 2)] public string NamaPemohon { get => nama_pemohon; set => nama_pemohon = value; }

        [DataMember(Order = 3)] public string AlamatPemohon { get => alamat_pemohon; set => alamat_pemohon = value; }

        [DataMember(Order = 4)] public int LuasTanah { get => luas_tanah; set => luas_tanah = value; }

        [DataMember(Order = 5)] public string LetakTanah { get => letak_tanah; set => letak_tanah = value; }

        [DataMember(Order = 6)] public string SuratKuasa { get => surat_kuasa; set => surat_kuasa = value; }

        [DataMember(Order = 7)] public string SertifikatAsli { get => sertifikat_asli; set => sertifikat_asli = value; }

        [DataMember(Order = 8)] public string AktaJB { get => akta_jb; set => akta_jb = value; }

        [DataMember(Order = 9)] public string KeteranganWrs { get => keterangan_warisan; set => keterangan_warisan = value; }

        [DataMember(Order = 10)] public string KeteranganMati { get => keterangan_kematian; set => keterangan_kematian = value; }

        [DataMember(Order = 11)] public string SSPBPHTP { get => sspbphtp; set => sspbphtp = value; }

        [DataMember(Order = 12)] public string PPH { get => pph; set => pph = value; }

        [DataMember(Order = 13)] public DateTime Tanggal { get => tanggal; set => tanggal = value; }
    }

    [DataContract]
    public class SuratKuasa
    {
        private string status;
        [DataMember(Order = 1)] public string Status { get => status; set => status = value; }
    }
    [DataContract]
    public class SertifikatAsli
    {
        private string status;
        [DataMember(Order = 1)] public string Status { get => status; set => status = value; }
    }

    [DataContract]
    public class AktaJB
    {
        private string status;
        [DataMember(Order = 1)] public string Status { get => status; set => status = value; }
    }

    [DataContract]
    public class KetWrs
    {
        private string status;
        [DataMember(Order = 1)] public string Status { get => status; set => status = value; }
    }

    [DataContract]
    public class KetMati
    {
        private string status;
        [DataMember(Order = 1)] public string Status { get => status; set => status = value; }
    }

    [DataContract]
    public class SSBPHTP
    {
        private string status;
        [DataMember(Order = 1)] public string Status { get => status; set => status = value; }
    }

    [DataContract]
    public class PPH
    {
        private string status;
        [DataMember(Order = 1)] public string Status { get => status; set => status = value; }
    }
}
