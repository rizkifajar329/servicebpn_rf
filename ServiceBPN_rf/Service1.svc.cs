﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ServiceBPN_rf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {

        SqlConnection conn = new SqlConnection("Data Source=DESKTOP-8MJVDUA;Initial Catalog=bpnApp; User ID=sa;Password=rf");

        public void deleteDataSWaris(SertifikatWarisan data)
        {
            string query = "delete from create_sertifikat_warisan where nik_pemohon = @nik";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@nik", data.NIKPemohon);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public List<AktaJB> getAktaJB()
        {
            throw new NotImplementedException();
        }

        public List<SertifikatWarisan> getDataSWarisan(string nik_pemohon)
        {
            List<SertifikatWarisan> swrs = new List<SertifikatWarisan>();

            string query = "select * from dbo.create_sertifikat_warisan where nik_pemohon like '%" + nik_pemohon + "%'";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                SertifikatWarisan swrss = new SertifikatWarisan();
                swrss.NIKPemohon = sdr[1].ToString();
                swrss.NamaPemohon = sdr[2].ToString();
                swrss.AlamatPemohon = sdr[3].ToString();
                swrss.LuasTanah = int.Parse(sdr[4].ToString());
                swrss.LetakTanah = sdr[5].ToString();
                swrss.SuratKuasa = sdr[6].ToString();
                swrss.SertifikatAsli = sdr[7].ToString();
                swrss.AktaJB = sdr[8].ToString();
                swrss.SSPBPHTP = sdr[9].ToString();
                swrss.KeteranganWrs = sdr[10].ToString();
                swrss.KeteranganMati = sdr[11].ToString();
                swrss.PPH = sdr[12].ToString();
                swrss.Tanggal = DateTime.Parse(sdr[13].ToString());

                swrs.Add(swrss);
            }
            conn.Close();
            return swrs;
        }

        public List<KetMati> getKetMati()
        {
            List<KetMati> kmati = new List<KetMati>();

            string query = "select * from dbo.keterangan_kematian";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                KetMati kmatii = new KetMati();

                kmatii.Status = sdr[1].ToString();

                kmati.Add(kmatii);
            }
            conn.Close();
            return kmati;
        }

        public List<KetWrs> getKetWrs()
        {
            List<KetWrs> kwrs = new List<KetWrs>();

            string query = "select * from dbo.keterangan_warisan";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                KetWrs kwrss = new KetWrs();

                kwrss.Status = sdr[1].ToString();

                kwrs.Add(kwrss);
            }
            conn.Close();
            return kwrs;
        }

        public List<PPH> getPPH()
        {
            List<PPH> pph = new List<PPH>();

            string query = "select * from dbo.pph";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                PPH pphh = new PPH();

                pphh.Status = sdr[1].ToString();

                pph.Add(pphh);
            }
            conn.Close();
            return pph;
        }

        public List<SertifikatAsli> getSertifikatAsli()
        {
            List<SertifikatAsli> sa = new List<SertifikatAsli>();

            string query = "select * from dbo.keterangan_sertifikat_asli";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                SertifikatAsli saa = new SertifikatAsli();

                saa.Status = sdr[1].ToString();

                sa.Add(saa);
            }
            conn.Close();
            return sa;
        }

        public List<SertifikatWarisan> getSertifikatWarisan()
        {
            List<SertifikatWarisan> swrs = new List<SertifikatWarisan>();

            string query = "select * from dbo.create_sertifikat_warisan";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                SertifikatWarisan swrss = new SertifikatWarisan();

                swrss.NIKPemohon = sdr[1].ToString();
                swrss.NamaPemohon = sdr[2].ToString();
                swrss.AlamatPemohon = sdr[3].ToString();
                swrss.LuasTanah = int.Parse(sdr[4].ToString());
                swrss.LetakTanah = sdr[5].ToString();
                swrss.SuratKuasa = sdr[6].ToString();
                swrss.SertifikatAsli = sdr[7].ToString();
                swrss.AktaJB = sdr[8].ToString();
                swrss.KeteranganWrs = sdr[9].ToString();
                swrss.KeteranganMati = sdr[10].ToString();
                swrss.SSPBPHTP = sdr[11].ToString();
                swrss.PPH = sdr[12].ToString();
                swrss.Tanggal = DateTime.Parse(sdr[13].ToString());

                swrs.Add(swrss);
            }
            conn.Close();


            return swrs;
        }

        public List<SSBPHTP> getSSBPHTP()
        {
            List<SSBPHTP> sspbphtp = new List<SSBPHTP>();

            string query = "select * from dbo.sspbphtb";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                SSBPHTP sspbphtpp = new SSBPHTP();

                sspbphtpp.Status = sdr[1].ToString();

                sspbphtp.Add(sspbphtpp);
            }
            conn.Close();
            return sspbphtp;
        }

        public List<SuratKuasa> getSuratKuasa()
        {
            List<SuratKuasa> sk = new List<SuratKuasa>();

            string query = "select * from dbo.surat_kuasa";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                SuratKuasa skk = new SuratKuasa();

                skk.Status = sdr[1].ToString();

                sk.Add(skk);
            }
            conn.Close();
            return sk;
        }

        public string inputSertifikatWarisan(SertifikatWarisan dataTanah)
        {
            string message = "";
            string query = "insert into create_sertifikat_warisan(nik_pemohon, nama_pemohon, alamat_pemohon, luas_tanah, letak_tanah," +
                "surat_kuasa, sertifikat_asli, akta_jb, keterangan_warisan, keterangan_kematian, sspbphtp, pph, tanggal) values " +
                "(@nik, @namapemilik, @alamat, @luas, @letak, @sk, @sertifikatasli, @aktajb, " +
                "@ketwrs, @ketmati, @sspbphtb, @pph, @tanggal)";

            SqlCommand cmd = new SqlCommand(query, conn);

            cmd.Parameters.AddWithValue("@nik", dataTanah.NIKPemohon);
            cmd.Parameters.AddWithValue("@namapemilik", dataTanah.NamaPemohon);
            cmd.Parameters.AddWithValue("@alamat", dataTanah.AlamatPemohon);
            cmd.Parameters.AddWithValue("@luas", dataTanah.LuasTanah);
            cmd.Parameters.AddWithValue("@letak", dataTanah.LetakTanah);
            cmd.Parameters.AddWithValue("@sk", dataTanah.SuratKuasa);
            cmd.Parameters.AddWithValue("@sertifikatasli", dataTanah.SertifikatAsli);
            cmd.Parameters.AddWithValue("@aktajb", dataTanah.AktaJB);
            cmd.Parameters.AddWithValue("@ketwrs", dataTanah.KeteranganWrs);
            cmd.Parameters.AddWithValue("@ketmati", dataTanah.KeteranganMati);
            cmd.Parameters.AddWithValue("@sspbphtb", dataTanah.SSPBPHTP);
            cmd.Parameters.AddWithValue("@pph", dataTanah.PPH);
            cmd.Parameters.AddWithValue("@tanggal", dataTanah.Tanggal);

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                message = "Query berhasil";
                conn.Close();
            }

            catch (Exception e)
            {
                message = "Koneksi fail" + e.Message;
            }

            return message;
        }

        public void updateSwaris(SertifikatWarisan data)
        {
            string query = "update create_sertifikat_warisan set nik_pemohon=@nik, nama_pemohon=@namapemilik,alamat_pemohon=@alamat, luas_tanah=@luas, " +
                            "letak_tanah=@letak, surat_kuasa=@sk, sertifikat_asli=@sa, akta_jb=@akta, keterangan_warisan=@kw, keterangan_kematian=@kk, " +
                            "sspbphtp=@sspb, pph=@pph, tanggal=@tanggal where nik_pemohon=@nik";

            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@nik", data.NIKPemohon);
            cmd.Parameters.AddWithValue("@namapemilik", data.NamaPemohon);
            cmd.Parameters.AddWithValue("@alamat", data.AlamatPemohon);
            cmd.Parameters.AddWithValue("@luas", data.LuasTanah);
            cmd.Parameters.AddWithValue("@letak", data.LetakTanah);
            cmd.Parameters.AddWithValue("@sk", data.SuratKuasa);
            cmd.Parameters.AddWithValue("@sa", data.SertifikatAsli);
            cmd.Parameters.AddWithValue("@akta", data.AktaJB);
            cmd.Parameters.AddWithValue("@kw", data.KeteranganWrs);
            cmd.Parameters.AddWithValue("@kk", data.KeteranganMati);
            cmd.Parameters.AddWithValue("@sspb", data.SSPBPHTP);
            cmd.Parameters.AddWithValue("@pph", data.PPH);
            cmd.Parameters.AddWithValue("@tanggal", data.Tanggal);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
}
